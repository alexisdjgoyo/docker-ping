FROM debian:latest
RUN apt update && apt install iputils-ping -y
CMD ["ping", "8.8.8.8"]