# docker-ping
 
heramienta de prueba de conectividad.
 
 
#### Repositorio
 
Clonar repositorio.
 
```
$ git clone https://gitlab.com/sergio.pernas2/docker-ping.git
 
$ cd docker-ping
```
 
 
### Dockerfile.simple
 
Este fichero contruye una imagen con el comando 'ping 8.8.8.8'.
 
Crear imagen.
 
```
$ docker build -f Dockerfile.simple -t image_name .